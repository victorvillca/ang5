'use strict';

/**
 * @ngdoc function
 * @name ang5App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ang5App
 */
angular.module('ang5App')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
