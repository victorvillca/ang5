'use strict';

/**
 * @ngdoc function
 * @name ang5App.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the ang5App
 */
angular.module('ang5App')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
